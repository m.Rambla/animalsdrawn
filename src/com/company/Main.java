package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner inScanner = new Scanner(System.in);
        boolean userInputHasFinished = false;

        while(!userInputHasFinished) {
            System.out.println("Quel animal voulez vous ajouter à votre troupeau (vache/cheval/chameau/rien) ?");
            String userInput = inScanner.nextLine();

            switch(userInput) {
                case "vache":
                    // Créer et ajouter une vache au troupeau
                    break;
                case "cheval":
                    // Créer et ajouter un cheval au troupeau
                    break;
                case "chameau":
                    // Créer et ajouter un chameau au troupeau
                    break;
                case "rien":
                    userInputHasFinished = true;
                    break;
                default:
                    System.out.println("Vous devez rentrer un choix valide !");
                    break;
            }
        }


        // L'utilisateur a fini de constituer son troupeau : afficher le résultat à la console

    }
}
